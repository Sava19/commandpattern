package ru.sav;

public class SendSMSAndEmailCommand implements Command {

    BackendSystem backend;
    String name;

    public SendSMSAndEmailCommand(BackendSystem backend, String name) {
        this.backend = backend;
        this.name = name;
    }

    @Override
    public void execute() {
        backend.sendSMSWithLink(name);
        backend.sendSMSNotification(name);
        backend.sendEmail(name);
    }
}
