package ru.sav;

public class Client {

    private String name;
    private NotificationType type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public Client(String name, NotificationType type) {
        this.name = name;
        this.type = type;
    }
}
