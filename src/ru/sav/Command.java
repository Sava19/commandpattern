package ru.sav;

public interface Command {

    void execute();
}
