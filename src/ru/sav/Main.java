package ru.sav;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class Main {

    public static void main(String[] args) {
	    BackendSystem backend = new BackendSystem();
	    Client client1 = new Client("���� ��������", NotificationType.SMS);
        Client client2 = new Client("ϸ�� ��������", NotificationType.EMAIL);
        Client client3 = new Client("���� ��������", NotificationType.BOTH);

        List<Client> clients = new ArrayList<>(asList(client1, client2, client3));
        Command command;

        for (Client cl: clients) {
            switch (cl.getType()) {
                case SMS:
                    command = new SendSMSCommand(backend, cl.getName());
                    break;
                case EMAIL:
                    command = new SendEmailCommand(backend, cl.getName());
                    break;
                case BOTH:
                    command = new SendSMSAndEmailCommand(backend, cl.getName());
                    break;
                default:
                    throw new RuntimeException("������������ ��� �����������");
            }
            new Invoker(command).execute();
            System.out.println("============================");
        }
    }

}
