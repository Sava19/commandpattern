package ru.sav;

public class SendEmailCommand implements Command {
    BackendSystem backend;
    String name;

    public SendEmailCommand(BackendSystem backend, String name) {
        this.backend = backend;
        this.name = name;
    }

    @Override
    public void execute() {
        backend.sendSMSNotification(name);
        backend.sendEmail(name);
    }
}
