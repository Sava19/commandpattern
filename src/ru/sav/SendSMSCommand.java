package ru.sav;

public class SendSMSCommand implements Command {
    BackendSystem backend;
    String name;

    public SendSMSCommand(BackendSystem backend, String name) {
        this.backend = backend;
        this.name = name;
    }

    @Override
    public void execute() {
        backend.sendSMSWithLink(name);
    }
}
